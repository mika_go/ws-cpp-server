# Chat Server

Server working on port 7777

WS URL: http://localhost:7777/ws

Supported message types:
 
1. Error (Server side) = 0
2. Ok (Server side) = 1
3. Auth (Public side) = 2
4. Register (Public side) = 3
5. Logout (Public side) = 4
6. Public (Public message) = 5
7. Private (Private client message) = 6
8. OnlineList (Current list of online users) = 7

Basic message structure:
```json
{
  "message_type": __MESSAGE_TYPE__,
  "payload": {...}
}
```

Example of Error:
```json
{
  "message_type": 0,
  "payload": {
    "code": 404,
    "message": "not found",
  }
}
```
Example of Ok:
```json
{
  "message_type": 1
}
```
Example of Auth:
```json
{
  "message_type": 2,
  "payload": {
    "nickname": "admin",
    "password": "admin"
  }
}
```
Example of Register:
```json
{
  "message_type": 3,
  "payload": {
    "nickname": "admin1",
    "password": "admin"
  }
}
```
Example of Logout:
```json
{
  "message_type": 4
}
```
Example of Public:
```json
{
  "message_type": 5,
  "payload": {
    "from": "admin",
    "message": "Hello world!",
    "is_forwarded": false // optional
  }
}
```
Example of Private:
```json
{
  "message_type": 6,
  "payload": {
    "from": "admin",
    "to": "admin1",
    "message": "Hello world!",
    "is_forwarded": false // optional
  }
}
```
Example of OnlineList:
```json
{
  "message_type": 7,
  "payload": {
    "users": [
      "admin1",
      "admin"
    ]
  }
}
```