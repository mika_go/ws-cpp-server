#include "Message.hpp"
#include <sstream>
#include <iostream>

#include <pqxx/pqxx>

Message::Message(std::stringstream &json, std::shared_ptr<WsServer::Connection> connection) {
    ptree pt;
    read_json(json, pt);
    _messageType = static_cast<MessageType>(pt.get<int>("message_type"));
    switch (_messageType) {
        case Auth:
        case Register:
            _payload = new AuthMessage;
            break;
        case Public:
            _payload = new PublicMessage;
            break;
        case Private:
            _payload = new PrivateMessage;
            break;
        case Logout: {
            connections_mutex.lock();
            connections.erase(connection);
            connections_mutex.unlock();

            sendOnlineList();
            auto[pt1, json] = Message(Ok).ToJson();
            connection->send(json);
            return;
        }
        default: {
            auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::client_error_bad_request, std::string("wrong message type"));
            Message message(Error, errorMessage.get());
            auto[pt, json] = message.ToJson();
            connection->send(json);
            return;
        }        
    }
    std::stringstream payload;
    write_json(payload, pt.get_child("payload"), false);
    _payload->FromJson(payload);

    switch (_messageType) {
        case Auth:
            try {
                auto authMessage = dynamic_cast<AuthMessage *>(_payload);
                pqxx::connection conn(pg_conn_string);
                pqxx::work W(conn);
                auto result = W.exec("SELECT password FROM users WHERE nickname = '" + authMessage->_nickname + "'");
                if (result.empty() || result[0][0].c_str() != authMessage->_password) {
                    auto errorMessage = std::make_shared<ErrorMessage>(
                            (int) SimpleWeb::StatusCode::client_error_unauthorized,
                            std::string("no user with this nickname or password"));
                    Message message(Error, errorMessage.get());
                    auto[pt, json] = message.ToJson();
                    connection->send(json);
                    return;
                }

                for (const auto &[key, value] : connections) {
                    if (value == authMessage->_nickname) {
                        auto errorMessage = std::make_shared<ErrorMessage>(
                                (int) SimpleWeb::StatusCode::client_error_bad_request,
                                std::string("user already logged in"));
                        Message message(Error, errorMessage.get());
                        auto[pt, json] = message.ToJson();
                        connection->send(json);
                        return;
                    }
                }

                connections_mutex.lock();
                connections[connection] = authMessage->_nickname;
                connections_mutex.unlock();

                sendOnlineList();
                auto[pt1, json] = Message(Ok).ToJson();
                connection->send(json);
            } catch (const std::exception &e) {
                std::cout << "Server: Error occurred \"" << e.what() << "\"" << std::endl;
                auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::server_error_bad_gateway, e.what());
                Message message(Error, errorMessage.get());
                auto[pt, json] = message.ToJson();
                connection->send(json);
            }

            break;
        case Register:
            try {
                auto authMessage = dynamic_cast<AuthMessage *>(_payload);
                pqxx::connection conn(pg_conn_string);
                pqxx::work W(conn);
                auto result = W.exec("SELECT * FROM users WHERE nickname = '" + authMessage->_nickname + "'");
                if (!result.empty()) {
                    auto errorMessage = std::make_shared<ErrorMessage>(
                            (int) SimpleWeb::StatusCode::client_error_unauthorized,
                            std::string("User with this nickname already exist"));
                    Message message(Error, errorMessage.get());
                    auto[pt, json] = message.ToJson();
                    connection->send(json);
                    return;
                }
                W.exec("INSERT INTO users(nickname, password) VALUES('" + authMessage->_nickname + "', '" +
                       authMessage->_password + "')");
                W.commit();

                auto[pt2, json] = Message(Ok).ToJson();
                connection->send(json);
            } catch (const std::exception &e) {
                std::cout << "Server: Error occurred \"" << e.what() << "\"" << std::endl;
                auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::server_error_bad_gateway, std::string(e.what()));
                Message message(Error, errorMessage.get());
                auto[pt, json] = message.ToJson();
                connection->send(json);
            }
            break;
        case Public: {
            if (connections.find(connection) == connections.end()) {
                auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::client_error_unauthorized, std::string("unauthorized client"));
                Message message(Error, errorMessage.get());
                auto[pt, json] = message.ToJson();
                connection->send(json);
                return;
            }
            auto[pt, json] = ToJson();
            for (const auto &[conn, nickname] : connections) {
                if (conn != connection) {
                    conn->send(json);
                }
            }
            auto[pt1, json1] = Message(Ok).ToJson();
            connection->send(json1);
            break;
        }
        case Private: {
            if (connections.find(connection) == connections.end()) {
                auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::client_error_unauthorized, std::string("unauthorized client"));
                Message message(Error, errorMessage.get());
                auto[pt, json] = message.ToJson();
                connection->send(json);
                return;
            }
            auto privateMessage = dynamic_cast<PrivateMessage *>(_payload);
            
            auto target = std::find_if(connections.begin(), connections.end(),
                                       [&privateMessage](const auto &value) {
                                           return value.second == privateMessage->_to;
                                       });

            if (target != connections.end()) {
                auto[pt, json] = ToJson();
                target->first->send(json);
            } else {
                auto errorMessage = std::make_shared<ErrorMessage>(
                        (int) SimpleWeb::StatusCode::client_error_bad_request, std::string("wrong destination"));
                Message message(Error, errorMessage.get());
                auto[pt, json] = message.ToJson();
                connection->send(json);
                return;
            }
            auto[pt1, json1] = Message(Ok).ToJson();
            connection->send(json1);
            break;
        }
        default:
            break;
    }
}

Message::Message(MessageType messageType, Marshaler *payload) {
    _messageType = messageType;
    _payload = payload;
}

std::tuple<ptree, std::string> Message::ToJson() {
    std::stringstream json;
    ptree pt;
    pt.put<int>("message_type", _messageType);
    if (_payload != nullptr) {
        auto[pt1, json] = _payload->ToJson();
        pt.put_child("payload", pt1);
    }
    write_json(json, pt, false);
    return {pt, json.str()};
}

void Message::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);
    _messageType = static_cast<MessageType>(pt.get<int>("message_type"));
    std::stringstream ss(pt.get<std::string>("payload"));
    _payload->FromJson(ss);
}

AuthMessage::AuthMessage(const std::string &nickname, const std::string &password) {
    _nickname = nickname;
    _password = password;
}

std::tuple<ptree, std::string> AuthMessage::ToJson() {
    std::stringstream json;
    ptree pt;
    pt.put("nickname", _nickname);
    pt.put("password", _password);
    write_json(json, pt, false);
    return {pt, json.str()};
}

void AuthMessage::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);
    _nickname = pt.get<std::string>("nickname");
    _password = pt.get<std::string>("password");
}

PublicMessage::PublicMessage(bool is_forwarded, const std::string &from, const std::string &message) {
    _is_forwarded = is_forwarded;
    _from = from;
    _message = message;
}

std::tuple<ptree, std::string> PublicMessage::ToJson() {
    std::stringstream json;
    ptree pt;

    if (_is_forwarded) pt.put<bool>("is_forwarded", _is_forwarded);
    pt.put("from", _from);
    pt.put("message", _message);
    write_json(json, pt, false);
    return {pt, json.str()};
}

void PublicMessage::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);
    _is_forwarded = pt.get<bool>("is_forwarded", false);
    _from = pt.get<std::string>("from");
    _message = pt.get<std::string>("message");
}

OnlineListMessage::OnlineListMessage(const std::vector<std::string> &users) {
    _users = users;
}

std::tuple<ptree, std::string> OnlineListMessage::ToJson() {
    std::stringstream json;
    ptree pt;
    ptree children;

    for (const auto &user: _users) {
        ptree child;
        child.put("", user);
        children.push_back(std::make_pair("", child));
    }

    pt.add_child("users", children);
    write_json(json, pt, false);
    return {pt, json.str()};
}

void OnlineListMessage::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);
    _users.clear();
    for (const auto &item : pt.get_child("users")) {
        _users.push_back(item.second.get_value<std::string>());
    }
}

ErrorMessage::ErrorMessage(int code, const std::string &message) {
    _code = code;
    _message = message;
}

std::tuple<ptree, std::string> ErrorMessage::ToJson() {
    std::stringstream json;
    ptree pt;
    pt.put<int>("code", _code);
    pt.put("message", _message);
    write_json(json, pt, false);
    return {pt, json.str()};
}

void ErrorMessage::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);
    _code = pt.get<int>("code");
    _message = pt.get<std::string>("message");
}

PrivateMessage::PrivateMessage(bool is_forwarded, const std::string &from, const std::string &to,
                               const std::string &message) {
    _is_forwarded = is_forwarded;
    _from = from;
    _to = to;
    _message = message;
}

std::tuple<ptree, std::string> PrivateMessage::ToJson() {
    std::stringstream json;
    ptree pt;
    if (_is_forwarded) pt.put<bool>("is_forwarded", _is_forwarded);
    pt.put("from", _from);
    pt.put("to", _to);
    pt.put("message", _message);
    write_json(json, pt, false);
    return {pt, json.str()};
}

void PrivateMessage::FromJson(std::stringstream &json) {
    ptree pt;
    read_json(json, pt);

    _is_forwarded = pt.get<bool>("is_forwarded", false);
    _from = pt.get<std::string>("from");
    _to = pt.get<std::string>("to");
    _message = pt.get<std::string>("message");
}

void sendOnlineList() {
    std::vector<std::string> users;
    for (const auto &[key, value] : connections) {
        users.emplace_back(value);
    }
    auto onlineListMessage = std::make_shared<OnlineListMessage>(users);
    Message message(OnlineList, onlineListMessage.get());
    auto[pt, messageJson] = message.ToJson();
    for (const auto &[key, value] : connections) {
        key->send(messageJson);
    }
}