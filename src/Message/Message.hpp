#ifndef CHAT_SERVER_MESSAGE_HPP
#define CHAT_SERVER_MESSAGE_HPP

#pragma once

#include <string>
#include <vector>
#include <server_ws.hpp>
#include <memory>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using namespace boost::property_tree;

enum MessageType {
    Error = 0, // 0
    Ok, // 1
    Auth, // 2
    Register, // 3
    Logout, // 4
    Public, // 5
    Private, // 6
    OnlineList, // 7
};

inline std::map<std::shared_ptr<WsServer::Connection>, std::string> connections;
inline std::mutex connections_mutex;

const std::string pg_conn_string("host=localhost port=5432 dbname=chat user=chat_admin password=admin");

void sendOnlineList();

class Marshaler {
public:
    Marshaler() = default;
    Marshaler(const Marshaler &) = default;
    virtual std::tuple<ptree, std::string> ToJson() = 0;

    virtual void FromJson(std::stringstream &json) = 0;
};

class Message : public Marshaler {
public:
    MessageType _messageType;
    Marshaler *_payload = nullptr;

    Message(std::stringstream &json, std::shared_ptr<WsServer::Connection> connection);

    explicit Message(MessageType messageType, Marshaler *_payload = nullptr);

    Message(const Message &) = default;

    std::tuple<ptree, std::string> ToJson() override;

    void FromJson(std::stringstream &json) override;
};

// Error payload
class ErrorMessage : public Marshaler {
public:
    int _code;
    std::string _message;

    ErrorMessage() = default;
    ErrorMessage(int code, const std::string &message);
    ErrorMessage(const ErrorMessage &) = default;

    std::tuple<ptree, std::string> ToJson() override;
    void FromJson(std::stringstream &json) override;
};

// Auth or Register payload
class AuthMessage : public Marshaler {
public:
    std::string _nickname;
    std::string _password;

    AuthMessage() = default;
    AuthMessage(const std::string &nickname, const std::string &password);

    AuthMessage(const AuthMessage &) = default;

    std::tuple<ptree, std::string> ToJson() override;

    void FromJson(std::stringstream &json) override;
};

// Public client message payload
class PublicMessage : public Marshaler {
public:
    bool _is_forwarded = false;
    std::string _from;
    std::string _message;

    PublicMessage() = default;

    PublicMessage(bool is_forwarded, const std::string &from, const std::string &message);

    PublicMessage(const PublicMessage &) = default;

    std::tuple<ptree, std::string> ToJson() override;

    void FromJson(std::stringstream &json) override;
};

// Private client message payload
class PrivateMessage : public Marshaler {
public:
    bool _is_forwarded = false;
    std::string _from;
    std::string _to;
    std::string _message;

    PrivateMessage() = default;
    PrivateMessage(bool is_forwarded, const std::string &from, const std::string &to,
                   const std::string &message);
    PrivateMessage(const PrivateMessage &) = default;

    std::tuple<ptree, std::string> ToJson() override;
    void FromJson(std::stringstream &json) override;
};

// List of online users
class OnlineListMessage : public Marshaler {
public:
    std::vector<std::string> _users;

    OnlineListMessage() {}

    explicit OnlineListMessage(const std::vector<std::string> &users);

    OnlineListMessage(const OnlineListMessage &) = default;

    std::tuple<ptree, std::string> ToJson() override;

    void FromJson(std::stringstream &json) override;
};

#endif //CHAT_SERVER_MESSAGE_HPP
