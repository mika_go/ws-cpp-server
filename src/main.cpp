#include <iostream>
#include <server_ws.hpp>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "Message/Message.hpp"

using namespace std;
using namespace boost::property_tree;

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;

int main() {
    WsServer wsServer;

    wsServer.config.port = 7777;
    auto &ws = wsServer.endpoint["^/ws/?$"];

    ws.on_open = [](shared_ptr<WsServer::Connection> connection) {
        cout << "Server: Opened connection " << connection.get() << endl;
    };

    ws.on_message = [](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::InMessage> in_message) {
        auto in_string = in_message->string();
        cout << "Server: Message received: \"" << in_string << "\" from " << connection.get() << endl;
        try {
            stringstream ss(in_string);
            Message(ss, connection);
        } catch (const exception &e) {
            cout << "Server: Error occurred \"" << e.what() << "\"" << endl;
            auto errorMessage = make_shared<ErrorMessage>(
                    (int) SimpleWeb::StatusCode::server_error_internal_server_error, string(e.what()));
            Message message(Error, errorMessage.get());
            auto[pt, json] = message.ToJson();
            connection->send(json);
        }
    };

    ws.on_close = [](std::shared_ptr<WsServer::Connection> connection, int code, const std::string & reason) {
        cout << "Server: Connection closed by " << connection.get() << " with code (" << code << ") and reason \"" << reason << "\"" << endl;
        connections_mutex.lock();
        auto removed = connections.erase(connection);
        connections_mutex.unlock();
        if (removed == 0) return;
        sendOnlineList();
    };

    ws.on_error = [](std::shared_ptr<WsServer::Connection> connection, const error_code & code) {
        cout << "Server: Get error from  " << connection.get() << " with code (" << code << ")" << endl;
        connections_mutex.lock();
        auto removed = connections.erase(connection);
        connections_mutex.unlock();
        if (removed == 0) return;
        sendOnlineList();
    };

    wsServer.start();
    return 0;
}